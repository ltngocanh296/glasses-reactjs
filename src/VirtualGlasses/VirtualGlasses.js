import React, { Component } from 'react'
import { dataGlasses } from './dataGlasses'
import DetailGlass from './DetailGlass'
import GlassList from './GlassList'


export default class VirtualGlasses extends Component {
  state = {
    glassArr: dataGlasses,
    virtualGlass: dataGlasses,
  }
  handleVirtualGlass = (value) => {
    this.setState({ virtualGlass: value})
  }

  render() {
    return (
        <div class="container py-2">
          <div class="row justify-content-between">
                  <div>
                      <div class="vglasses__model"></div>
                  </div>
                  <div>
                      <div class="vglasses__model">
                        <DetailGlass virtualGlass = {this.state.virtualGlass} ></DetailGlass>
                      </div>
                      
                  </div>
          </div>
          <GlassList
            handleVirtualGlass = {this.handleVirtualGlass}
            glassArr = {this.state.glassArr}>
          </GlassList>
        </div>
    )
  }
}
