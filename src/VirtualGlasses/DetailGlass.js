import React, { Component } from 'react'

export default class DetailGlass extends Component {
  render() {
    return (
        <div class="vglasses__info">
            <div className="glassChoice"><img src={this.props.virtualGlass.url} alt="" /></div>
            <div className="detailGlass">
            <p>{this.props.virtualGlass.name}</p>
            <p>{this.props.virtualGlass.price}</p>
            <p>{this.props.virtualGlass.desc}</p>
            </div>
        </div>
    )
  }
}
