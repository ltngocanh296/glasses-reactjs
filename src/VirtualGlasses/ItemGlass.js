import React, { Component } from 'react'

export default class ItemGlass extends Component {
  render() {
    let {url } = this.props.data;
    return (
      <div className='col-4 imgGlass'><img onClick={()=>{this.props.handleVirtualGlass(this.props.data)}} src={url} alt="" /></div>
    )
  }
}
