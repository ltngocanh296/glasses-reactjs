import logo from './logo.svg';
import './App.css';
import VirtualGlasses from './VirtualGlasses/VirtualGlasses';

function App() {
  return (
    <VirtualGlasses></VirtualGlasses>
  );
}

export default App;
